export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      predictor_actual: {
        Row: {
          actual: string
          created_at: string | null
          id: number
        }
        Insert: {
          actual: string
          created_at?: string | null
          id?: number
        }
        Update: {
          actual?: string
          created_at?: string | null
          id?: number
        }
        Relationships: []
      }
      predictor_predictions: {
        Row: {
          created_at: string | null
          favorite_team: number
          id: number
          identifier: string
          name: string
          prediction: string
        }
        Insert: {
          created_at?: string | null
          favorite_team?: number
          id?: number
          identifier: string
          name: string
          prediction: string
        }
        Update: {
          created_at?: string | null
          favorite_team?: number
          id?: number
          identifier?: string
          name?: string
          prediction?: string
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
