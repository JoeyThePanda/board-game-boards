import type { Metadata } from "next";
import { Atkinson_Hyperlegible } from "next/font/google";

import "./globals.css";

const inter = Atkinson_Hyperlegible({ subsets: ["latin"], weight: "400" });

export const metadata: Metadata = {
  description: "Created by Joey",
  title: "Board Game Boards",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
